package ru.t1.ktitov.tm.service;

import ru.t1.ktitov.tm.api.repository.IUserRepository;
import ru.t1.ktitov.tm.api.service.IUserService;
import ru.t1.ktitov.tm.enumerated.Role;
import ru.t1.ktitov.tm.exception.field.*;
import ru.t1.ktitov.tm.exception.user.UserEmailExistsException;
import ru.t1.ktitov.tm.exception.user.UserLoginExistsException;
import ru.t1.ktitov.tm.exception.entity.UserNotFoundException;
import ru.t1.ktitov.tm.model.User;
import ru.t1.ktitov.tm.util.HashUtil;

import java.util.List;

public final class UserService implements IUserService {

    private final IUserRepository userRepository;

    public UserService(IUserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public User create(String login, String password) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (isLoginExist(login)) throw new UserLoginExistsException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        return userRepository.create(login, password);
    }

    @Override
    public User create(String login, String password, String email) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (isLoginExist(login)) throw new UserLoginExistsException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        if (isEmailExist(email)) throw new UserEmailExistsException();
        return userRepository.create(login, password, email);
    }

    @Override
    public User create(String login, String password, Role role) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (isLoginExist(login)) throw new UserLoginExistsException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        if (role == null) throw new EmptyRoleException();
        return userRepository.create(login, password, role);
    }

    @Override
    public User add(User user) {
        if (user == null) throw new UserNotFoundException();
        return userRepository.add(user);
    }

    @Override
    public List<User> findAll() {
        return userRepository.findAll();
    }

    @Override
    public User findById(String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return userRepository.findById(id);
    }

    @Override
    public User findByLogin(String login) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        return userRepository.findByLogin(login);
    }

    @Override
    public User findByEmail(String email) {
        if (email == null || email.isEmpty()) throw new EmptyEmailException();
        return userRepository.findByEmail(email);
    }

    @Override
    public User remove(User user) {
        if (user == null) throw new UserNotFoundException();
        return userRepository.remove(user);
    }

    @Override
    public User removeById(String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        final User user = findById(id);
        if (user == null) throw new UserNotFoundException();
        return remove(user);
    }

    @Override
    public User removeByLogin(String login) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        final User user = findByLogin(login);
        if (user == null) throw new UserNotFoundException();
        return remove(user);
    }

    @Override
    public User removeByEmail(String email) {
        if (email == null || email.isEmpty()) throw new UserNotFoundException();
        final User user = findByEmail(email);
        if (user == null) throw new UserNotFoundException();
        return remove(user);
    }

    @Override
    public User setPassword(String id, String password) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        final User user = findById(id);
        if (user == null) throw new UserNotFoundException();
        user.setPasswordHash(HashUtil.salt(password));
        return user;
    }

    @Override
    public User updateUser(
            String id,
            String firstName,
            String lastName,
            String middleName
    ) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        final User user = findById(id);
        if (user == null) throw new UserNotFoundException();
        user.setFirstName(firstName);
        user.setLastName(lastName);
        user.setMiddleName(middleName);
        return user;
    }

    @Override
    public Boolean isLoginExist(String login) {
        if (login == null || login.isEmpty()) return false;
        return userRepository.isLoginExist(login);
    }

    @Override
    public Boolean isEmailExist(String email) {
        if (email == null || email.isEmpty()) return false;
        return userRepository.isEmailExist(email);
    }

}
