package ru.t1.ktitov.tm.command.user;

import ru.t1.ktitov.tm.model.User;

public final class UserViewProfileCommand extends AbstractUserCommand {

    public static final String NAME = "user-view-profile";

    public static final String DESCRIPTION = "View profile of current user";

    @Override
    public void execute() {
        final User user = getAuthService().getUser();
        System.out.println("CURRENT USER:");
        showUser(user);
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}
