package ru.t1.ktitov.tm.exception.field;

public final class EmptyTaskIdException extends AbstractFieldException {

    public EmptyTaskIdException() {
        super("Error! Task id is empty.");
    }

}
