package ru.t1.ktitov.tm.api.service;

import ru.t1.ktitov.tm.enumerated.Sort;
import ru.t1.ktitov.tm.enumerated.Status;
import ru.t1.ktitov.tm.model.Project;

import java.util.Comparator;
import java.util.List;

public interface IProjectService {

    Project add(Project project);

    Project create(String name);

    void create(String name, String description);

    void clear();

    List<Project> findAll();

    List<Project> findAll(Sort sort);

    List<Project> findAll(Comparator comparator);

    void remove(Project project);

    Project findOneById(String id);

    Project findOneByIndex(Integer index);

    void updateById(String id, String name, String description);

    void updateByIndex(Integer index, String name, String description);

    void changeProjectStatusById(String id, Status status);

    void changeProjectStatusByIndex(Integer index, Status status);

    Project removeById(String id);

    Project removeByIndex(Integer index);

}
