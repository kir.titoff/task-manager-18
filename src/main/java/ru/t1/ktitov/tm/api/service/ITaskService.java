package ru.t1.ktitov.tm.api.service;

import ru.t1.ktitov.tm.enumerated.Sort;
import ru.t1.ktitov.tm.enumerated.Status;
import ru.t1.ktitov.tm.model.Task;

import java.util.Comparator;
import java.util.List;

public interface ITaskService {

    Task add(Task task);

    Task create(String name);

    void create(String name, String description);

    void clear();

    List<Task> findAll();

    List<Task> findAll(Sort sort);

    List<Task> findAll(Comparator comparator);

    List<Task> findAllByProjectId(String projectId);

    void remove(Task task);

    Task findOneById(String id);

    Task findOneByIndex(Integer index);

    void updateById(String id, String name, String description);

    void updateByIndex(Integer index, String name, String description);

    void changeTaskStatusById(String id, Status status);

    void changeTaskStatusByIndex(Integer index, Status status);

    Task removeById(String id);

    void removeByIndex(Integer index);

}
